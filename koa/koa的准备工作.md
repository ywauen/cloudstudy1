**koa的准备工作**

1.安装NodeJS

2.准备工作目录

3.在工作目录下打开命令行并运行

   npm init --yes 初始化

   (node -v     查看nodeJS的版本号  npm-v      查看npm的版本号)

   npm install koa koa-router --save  安装koa

**安装过程**：

PS D:\nodeJS\koa-app> npm init --yes
Wrote to D:\nodeJS\koa-app\package.json:

{
  "name": "koa-app",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
}

PS D:\nodeJS\koa-app> cnpm install koa koa-router --save

+ cnpm install koa koa-router --save
+ ~~~~
    + CategoryInfo          : ObjectNotFound: (cnpm:String) [], CommandNotFoundException
    + FullyQualifiedErrorId : CommandNotFoundException

PS D:\nodeJS\koa-app> npm install koa koa-router --save
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN koa-app@1.0.0 No description
npm WARN koa-app@1.0.0 No repository field.

+ koa@2.13.1
+ koa-router@10.0.0
added 52 packages from 25 contributors and audited 52 packages in 15.249s

3 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities



  安装好之后的项目目录如下：

![image-20210820204431393](images/image-20210820204431393.png)

这个app.js就是主文件（可以在packege.json里面改，这里原本不是app.js，我给他改了一下）

![image-20210820204733073](images/image-20210820204733073.png)

到这里基本环境就安装完毕了。

下面是koa的实例化，然后就可以使用了。

![image-20210820205212376](images/image-20210820205212376.png)



**远程搭建mongoDB**

在mongo atals 里面创建一个500MB的免费数据库，然后在本地安装mongoDB  

```shell
npm install mongoose --save
```

在mongoDB集群的管理界面点击connect  选择第二个选项(connect your application)，点进去

![image-20210822140814446](images/image-20210822140814446.png)

```shell
mongodb+srv://root:<password>@cluster0.oylf1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority
```

就可以拿到这个url，只是把 <password>换成你自己设置的密码就行了。

然后在app.js里面写：

```javascript
mongoose.connect("mongodb+srv://root:caonima1998@cluster0.oylf1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
```

这里的密码我已经换了哈。

然后使用nodemon运行程序 ，不过事先需要在全局安装nodemon

```shell
npm install -g nodemon
```

然后直接执行

```shell
nodemon
```

就可以了。

**另外，可以把URL写在配置文件里面**

```javascript
module.exports={
    mongoURL: 'mongodb+srv://root:caonima1998@cluster0.oylf1.mongodb.net/test?retryWrites=true&w=majority&useUnifiedTopology=true'
}
```

然后在app.js里面引过来

```javascript
const db=require("./config/keys").mongoURL
```

之后需要用到url的地方，全部使用db这个变量来替代就可以了


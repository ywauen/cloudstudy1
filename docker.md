**Docker**

作为一种新兴的虚拟化方式，Docker 跟传统的虚拟化方式相比具有众多的优势。

（1）更高效的利用系统资源。

由于容器不需要进行硬件虚拟以及运行完整操作系统等额外开销，Docker 对系统资源的利用率更高。无论是应用执行速度、内存损耗或者文件存储速度，都要比传统虚拟机技术更高效。因此，相比虚拟机技术，一个相同配置的主机，往往可以运行更多数量的应用。

（2）更快速的启动时间

传统的虚拟机技术启动应用服务往往需要数分钟，而 Docker 容器应用，由于直接运行于宿主内核，无需启动完整的操作系统，因此可以做到秒级、甚至毫秒级的启动时间。大大的节约了开发、测试、部署的时间。

（3）一致的运行环境

开发过程中一个常见的问题是环境一致性问题。由于开发环境、测试环境、生产环境不一致，导致有些 bug 并未在开发过程中被发现。而 Docker 的镜像提供了除内核外完整的运行时环境，确保了应用运行环境一致性，从而不会再出现 「这段代码在我机器上没问题啊」 这类问题。

（4）持续交付和部署

对开发和运维（DevOps）人员来说，最希望的就是一次创建或配置，可以在任意地方正常运行。

使用 Docker 可以通过定制应用镜像来实现持续集成、持续交付、部署。开发人员可以通过 Dockerfile 来进行镜像构建，并结合 持续集成(Continuous Integration) 系统进行集成测试，而运维人员则可以直接在生产环境中快速部署该镜像，甚至结合 持续部署(Continuous Delivery/Deployment) 系统进行自动部署。

而且使用 Dockerfile 使镜像构建透明化，不仅仅开发团队可以理解应用运行环境，也方便运维团队理解应用运行所需条件，帮助更好的生产环境中部署该镜像。

（5）更轻松的迁移

由于 Docker 确保了执行环境的一致性，使得应用的迁移更加容易。Docker 可以在很多平台上运行，无论是物理机、虚拟机、公有云、私有云，甚至是笔记本，其运行结果是一致的。因此用户可以很轻易的将在一个平台上运行的应用，迁移到另一个平台上，而不用担心运行环境的变化导致应用无法正常运行的情况。

（6）更轻松的维护和扩展

Docker 使用的分层存储以及镜像的技术，使得应用重复部分的复用更为容易，也使得应用的维护更新更加简单，基于基础镜像进一步扩展镜像也变得非常简单。此外，Docker 团队同各个开源项目团队一起维护了一大批高质量的 官方镜像，既可以直接在生产环境使用，又可以作为基础进一步定制，大大的降低了应用服务的镜像制作成本。
![Image text](https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tbWJpei5xcGljLmNuL21tYml6X3BuZy9SMDZoM2U5aWN4UlhpYWpjcTZlaEthMXVTZ2dGRHBpY1ZpY3JGNFpvc0I5dmlhUmVxY01JTHh4UWZPYTBQSWptcWNFaWJsYWh3dFFSYnVpYkRoT1dCQXVGMnlXM0EvNjQw?x-oss-process=image/format,png)

Docker 组件

  Docker服务器与客户端

  Docker是一个客户端-服务器（C/S）架构程序。Docker客户端只需要向Docker服务器或者守护进程发出请求，服务器或者守护进程将完成所有工作并返回结果。Docker提供了一个命令行工具Docker以及一整套RESTful API。你可以在同一台宿主机上运行Docker守护进程和客户端，也可以从本地的Docker客户端连接到运行在另一台宿主机上的远程Docker守护进程。
docker client是一个泛称，它可以是命令行docker，也可以是遵循了docker api规则的客户端，简单地说可以理解为一个用于交互/发送指令的接口。

1.5.2 Docker镜像
镜像是构建Docker的基石。用户基于镜像来运行自己的容器。镜像也是Docker生命周期中的“构建”部分。镜像是基于联合文件系统的一种层式结构，由一系列指令一步一步构建出来。例如：

添加一个文件;
执行一个命令;
打开一个窗口

也可以将镜像当作容器的“源代码”。镜像体积很小，非常“便携”，易于分享、存储和更新。
镜像是一个只读的容器模板，含有启动docker容器所需的文件系统结构及内容 Docker以镜像和在镜像基础上构建的容器为基础，以容器开发、测试、发布的单元将应用相关的所有组件和环境进行封装，避免了应用在不同平台间迁移所带来的依赖问题，确保了应用在生产环境的各阶段达到高度一致的实际效果。
镜像可以被创建、启动、关闭、重启以及销毁。

分层机制

Docker的镜像机制是有层次感的，一个镜像可以放到另一个镜像的顶部。位于下端的为父镜像，以此类推；最底部的镜像可称为基础镜像。
镜像采用分层构建，每个镜像由一系列的镜像层组成, 当需要修改容器内的某个文件时，只对处于最上方的读写层进行变动，不覆盖下面已有文件系统的内容。当提交这个修改过的容器文件系统为一个新的镜像时，保存的内容仅为最上层读写文件系统中被更新过的文件。

 
bootfs

主要包含bootloader和kernel, bootloader主要是引导加载kernel, 当容器启动成功后，kernel被加载到内存中后而引导文件系统则会被卸载unmount+ rootfs  是容器在启动时内部进程可见的文件系统，通常包含一个操作系统运行所需的文件系统
传统linux在内核启动时首先会挂载一个只读的rootfs，检测器完整性之后再切换为读写模式
docker在挂载rootfs时也将其设为只读模式，挂载完毕后利用联合挂载技术在已有的只读rootfs上再挂载一个读写层。
只有运行中文件系统发生变化，才会把变化的内容写到读写层，并隐藏只读层中的老版本文件
rootfs包含的就是典型Linux系统中的 /dev,/proc,/bin, /etc等标准目录和文件。

写时复制

可以在多个容器之间共享镜像，每个容器启动时不需要单独复制一份镜像文件
将所有镜像层以只读方式挂载到一个挂载点，在上面覆盖一个可读写的容器层。
写时复制配合分层机制减少了镜像对磁盘空间的占用和容器启动时间

内容寻址

根据内容来索引镜像和镜像层
是对镜像层的内容计算检验和，生成一个内容哈希值作为镜像层的唯一标识
对于来自不同构建的镜像层，只要拥有相同的内容哈希，也能被不同镜像共享

联合挂载

可以在一个挂载点挂载多个文件系统，将挂载点的原目录与被挂在内容进行整合，最终可见的文件系统将包含整合后各层的文件和目录
读写层处于容器文件系统的最顶层，其下可能联合挂载多个只读层。

**Docker环境搭建**（基于centos）
使用官方安装脚本自动安装
安装命令如下：

curl -fsSL https://get.docker.com | bash -s docker --mirror aliyun
也可以使用国内 daocloud 一键安装命令：

curl -sSL https://get.daocloud.io/docker | sh
手动安装
卸载旧版本
较旧的 Docker 版本称为 docker 或 docker-engine 。如果已安装这些程序，请卸载它们以及相关的依赖项。

$ sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
安装 Docker Engine-Community
使用 Docker 仓库进行安装
在新主机上首次安装 Docker Engine-Community 之前，需要设置 Docker 仓库。之后，您可以从仓库安装和更新 Docker。

设置仓库

安装所需的软件包。yum-utils 提供了 yum-config-manager ，并且 device mapper 存储驱动程序需要 device-mapper-persistent-data 和 lvm2。

$ sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
使用以下命令来设置稳定的仓库。

使用官方源地址（比较慢）
$ sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
可以选择国内的一些源地址：

阿里云
$ sudo yum-config-manager \
    --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
清华大学源
$ sudo yum-config-manager \
    --add-repo \
    https://mirrors.tuna.tsinghua.edu.cn/docker-ce/linux/centos/docker-ce.repo
安装 Docker Engine-Community
安装最新版本的 Docker Engine-Community 和 containerd，或者转到下一步安装特定版本：

$ sudo yum install docker-ce docker-ce-cli containerd.io
如果提示您接受 GPG 密钥，请选是。

有多个 Docker 仓库吗？

如果启用了多个 Docker 仓库，则在未在 yum install 或 yum update 命令中指定版本的情况下，进行的安装或更新将始终安装最高版本，这可能不适合您的稳定性需求。

Docker 安装完默认未启动。并且已经创建好 docker 用户组，但该用户组下没有用户。

要安装特定版本的 Docker Engine-Community，请在存储库中列出可用版本，然后选择并安装：

1、列出并排序您存储库中可用的版本。此示例按版本号（从高到低）对结果进行排序。

$ yum list docker-ce --showduplicates | sort -r

docker-ce.x86_64  3:18.09.1-3.el7                     docker-ce-stable
docker-ce.x86_64  3:18.09.0-3.el7                     docker-ce-stable
docker-ce.x86_64  18.06.1.ce-3.el7                    docker-ce-stable
docker-ce.x86_64  18.06.0.ce-3.el7                    docker-ce-stable
2、通过其完整的软件包名称安装特定版本，该软件包名称是软件包名称（docker-ce）加上版本字符串（第二列），从第一个冒号（:）一直到第一个连字符，并用连字符（-）分隔。例如：docker-ce-18.09.1。

$ sudo yum install docker-ce-<VERSION_STRING> docker-ce-cli-<VERSION_STRING> containerd.io
启动 Docker。

$ sudo systemctl start docker
通过运行 hello-world 映像来验证是否正确安装了 Docker Engine-Community 。

$ sudo docker run hello-world
卸载 docker
删除安装包：

yum remove docker-ce
删除镜像、容器、配置文件等内容：

rm -rf /var/lib/docker


